Import("env")

result = env.Execute("$PYTHONEXE -m pip install python-dotenv==1.0.1")

if result != 0:
    print("Failed to install the required package. Stopping the build.")
    env.Exit(1)

import os
from dotenv import dotenv_values

dotenv_env = dotenv_values(".env")

IS_CI_ENVIRONMENT = os.getenv("CI") is not None

IS_UNPROTECTED_CI_BRANCH = (
    IS_CI_ENVIRONMENT and os.getenv("CI_COMMIT_REF_NAME") != "main"
)


def get_env_variable(name, default_value=None):
    if IS_UNPROTECTED_CI_BRANCH:
        return os.getenv(name) or dotenv_env.get(name) or default_value
    return os.getenv(name) or dotenv_env.get(name)

wifi_ssid = get_env_variable("WIFI_SSID")
wifi_password = get_env_variable("WIFI_PASSWORD", "SOMEPASS")
version = get_env_variable("VERSION") if IS_CI_ENVIRONMENT else "dev"
wifi_upload = get_env_variable("WIFI_UPLOAD")
server_host = get_env_variable("SERVER_HOST")
server_host_secondary = server_host if IS_CI_ENVIRONMENT else "cylon.in.hsbxl.be"
server_port = get_env_variable("SERVER_PORT")

if (
    not wifi_ssid
    or not wifi_password
    or not version
    or not server_host
    or not server_port
):
    print("WIFI_SSID, WIFI_PASSWORD, VERSION, SERVER_HOST or SERVER_PORT is not defined. Stopping the build.")
    env.Exit(1)

env.Append(
    CPPDEFINES=[
        ("WIFI_SSID", f'"\\"{wifi_ssid}\\""'),
        ("WIFI_PASSWORD", f'"\\"{wifi_password}\\""'),
        ("VERSION", f'"\\"{version}\\""'),
        ("SERVER_HOST", f'"\\"{server_host}\\""'),
        ("SERVER_HOST_SECONDARY", f'"\\"{server_host_secondary}\\""'),
        ("SERVER_PORT", server_port),
    ]
)

if wifi_upload:
    env.Replace(UPLOAD_PORT=wifi_upload)
    env.Replace(UPLOAD_PROTOCOL='espota')
    env.Append(UPLOAD_FLAGS=['--host_port=8266'])
