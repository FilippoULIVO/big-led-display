Import("env")

result = env.Execute("$PYTHONEXE -m pip install pillow==10.2.0")

if result != 0:
    print("Failed to install the required package. Stopping the build.")
    env.Exit(1)

from PIL import Image

# Replace 'input.png' with your input image file name
input_image_path = 'src/font.png'
# Output data file name
output_data_path = '.pio/build/font.bin'

# Open the image and convert it to binary (mode '1')
image = Image.open(input_image_path).convert('1')

# Ensure the image is 7 pixels tall
if image.height != 7:
    raise ValueError("Image must be exactly 7 pixels tall")

# Open the output file in binary write mode
with open(output_data_path, 'wb') as output_file:
    # Iterate over each column in the image
    for x in range(image.width):
        # Skip every 6th column, it contains empty space between characters
        if (x + 1) % 6 == 0:
            continue
        
        byte = 0
        # Iterate over each pixel in the column in reverse
        for y in range(image.height - 1, -1, -1):
            byte <<= 1
            pixel = 1 if image.getpixel((x, y)) else 0
            byte |= pixel
        # Write the byte to the file
        output_file.write(bytes([byte]))

print(f"Converted {input_image_path} to raw data file {output_data_path}")
