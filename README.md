# Big LED Display Driver

**Author:** Frederic Pasteleurs <frederic@askarel.be>  
**Year:** 2018  
**Board:** [WeMos D1 Mini](https://www.wemos.cc/en/latest/d1/d1_mini.html)

## Overview

This LED display driver is designed to operate large LED matrices with efficiency and simplicity. Utilizing shift registers for multiplexing, the driver controls the displays by lighting up rows sequentially.

## Getting Started

This guide provides instructions for using the LED display driver within PlatformIO, supporting both IDE integrations and a Command Line Interface (CLI), accommodating different developer preferences.

### Installing PlatformIO

1. **Visit PlatformIO's Website:** Find detailed installation guidelines at [PlatformIO's official website](https://platformio.org/). PlatformIO is compatible with various code editors like Visual Studio Code, Atom, Sublime Text, and CLion, and offers a robust CLI.

2. **Installation Options:**

   - **IDE Integration:** Install the PlatformIO IDE extension or plugin in your preferred editor for a graphical interface, which includes smart code completion, project management, and debugging.
   - **CLI Tool:** For command-line enthusiasts, PlatformIO Core (CLI) provides comprehensive functionalities for project management, library installation, board configuration, etc.

3. **Opening the Project:**
   - **Through IDE:** Open the project via the PlatformIO home screen in your code editor, selecting the project directory.
   - **Using CLI:** In the terminal, navigate to your project's directory and use PlatformIO commands for project management.

### Using CLI for Firmware Upload

To upload firmware using the CLI, navigate to the project directory and execute:

```bash
pio run -t upload
```

This command compiles and uploads the firmware to your board.

## Project Configuration

The project is pre-configured for the 'D1 Mini Pro' board in the `platformio.ini` file for easy setup and immediate use.

Here's a simplified version that specifies which tool to use for formatting C++ code and mentions the availability of IDE extensions:

## Code Formatting

We enforce consistent formatting across our codebase using **Clang-Format** for C++ and **Prettier** for JavaScript. These tools help maintain high standards of readability and consistency.

### Using Clang-Format for C++

Clang-Format automatically formats C++ code according to predefined styles and guidelines.

- **IDE Integration:** Most popular IDEs like Visual Studio Code, CLion, and Atom offer extensions or plugins for Clang-Format. These can be easily configured to format your code automatically.

### Using Prettier for JavaScript

Prettier is an opinionated code formatter that ensures that all outputted JavaScript code conforms to a consistent style.

- **IDE Integration:** You can integrate Prettier into editors like Visual Studio Code, Sublime Text, and Atom with plugins that format on save or on command.
- **Running Prettier:** We have a pre-configured script in our `package.json`:
  To format your JavaScript files, run:

  ```bash
  npm run lint:fix
  ```

## Matrix Display and Driver Configuration

The daisy-chained displays, managed through hardware SPI, allow for efficient data communication and minimal pin usage, with the HEF4094BP chip central to the data control and flow in the system.

### SPI Configuration

- **MOSI** (Master Out, Slave In): Facilitates data sending to the displays.
- **SCK** (Serial Clock): Provides the clock signal for SPI.
- **SS** (Slave Select): An optional pin for device selection on the SPI bus.
