import { createClient } from "matrix-js-sdk";

import { MATRIX_SERVER } from "./config.js";

const client = createClient({
  baseUrl: MATRIX_SERVER,
});

const userInfo = await client.registerGuest({
  body: {
    initial_device_display_name: "LED Matrix Display",
  },
});

console.log({
  userID: userInfo.user_id,
  accessToken: userInfo.access_token,
});
