import { WebSocket } from "ws";

const flashFirmware = (socket: WebSocket, firmware: Buffer) => {
  console.log("Flashing firmware");
  const length = firmware.byteLength;
  socket.send(
    Buffer.from([
      0x66,
      (length >> (3 * 8)) & 0xff,
      (length >> (2 * 8)) & 0xff,
      (length >> (1 * 8)) & 0xff,
      (length >> (0 * 8)) & 0xff,
    ]),
  );
  const CHUNK_SIZE = 4 * 1024;
  let offset = 0;
  while (offset < length) {
    const chunk = firmware.subarray(offset, offset + CHUNK_SIZE);
    socket.send(chunk);
    offset += CHUNK_SIZE;
  }
};

export default flashFirmware;
