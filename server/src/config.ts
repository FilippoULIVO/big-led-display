import { config } from "dotenv";

config({ path: "../.env" });

const IS_PRODUCTION = process.env.NODE_ENV === "production";
const matrixServer = process.env.MATRIX_SERVER;
if (!matrixServer) {
  throw new Error("MATRIX_SERVER is not defined");
}
export const MATRIX_SERVER = matrixServer;

const matrixUserId = process.env.MATRIX_USER_ID;
if (!matrixUserId) {
  throw new Error("MATRIX_USER_ID is not defined");
}
export const MATRIX_USER_ID = matrixUserId;

const matrixAccessToken = process.env.MATRIX_ACCESS_TOKEN;
if (!matrixAccessToken) {
  throw new Error("MATRIX_ACCESS_TOKEN is not defined");
}
export const MATRIX_ACCESS_TOKEN = matrixAccessToken;

const matrixChatroom = process.env.MATRIX_CHATROOM;
if (!matrixChatroom) {
  throw new Error("MATRIX_CHATROOM is not defined");
}
export const MATRIX_CHATROOM = matrixChatroom;

const version = IS_PRODUCTION ? process.env.VERSION : "dev";
if (!version) {
  throw new Error("VERSION is not defined");
}
export const VERSION = version;
