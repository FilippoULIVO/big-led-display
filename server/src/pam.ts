import { defer, distinctUntilChanged, from, retry, tap } from "rxjs";

const PAM_URL = "https://pam.hsbxl.be/data.php";
const IGNORED_DEVICES = [
  "gate",
  "LED-Display",
  "xbmc-eleclab",

  // transient devices
  "raspberrypi",
];
const POLL_INTERVAL = 60_000;

const ignoredDevicesSet = new Set(IGNORED_DEVICES);

const activeDevicesStream = defer(() =>
  from(
    (async function* generator() {
      while (true) {
        const response = await fetch(PAM_URL);
        const data: string[] = await response.json();
        yield data.some((device) => !ignoredDevicesSet.has(device));
        await new Promise((resolve) => setTimeout(resolve, POLL_INTERVAL));
      }
    })(),
  ).pipe(
    tap({
      error: (e) => {
        console.warn("PAM error", e);
      },
    }),
  ),
).pipe(
  retry({
    delay: POLL_INTERVAL,
  }),
  distinctUntilChanged(),
);

export default activeDevicesStream;
