#include <SPI.h>
#include <cstdint>

#define LINELENGTH 150
#define SCROLL_SPEED 30.0 
int SCROLL_OFFSET = 0;



enum Line { LINE_TOP, LINE_BOTTOM };

#define FRAMEBUFFER_COUNT 3

unsigned int framebuffer_offset = 0;
unsigned int framebuffers_used = 2;

uint8_t framebuffer[LINELENGTH * FRAMEBUFFER_COUNT];

#define ALIGN_BUFFER 4
#define DATA_TRANSFER_LENGTH (ALIGN_BUFFER + 2 * (LINELENGTH + 8))

bool getBitFromArray(const uint8_t *data, int data_idx) {
  int byteIndex = data_idx / 8;
  int bitIndex = data_idx % 8;

  return (data[byteIndex] & (1 << bitIndex)) != 0;
}

IRAM_ATTR
void setBitInArray(uint8_t *data, int data_idx, bool value) {
  int byteIndex = data_idx / 8;
  int bitIndex = data_idx % 8;

  if (value) {
    data[byteIndex] |= (1 << bitIndex); // Set the bit
  } else {
    data[byteIndex] &= ~(1 << bitIndex); // Clear the bit
  }
}

volatile int active_line = 0;


IRAM_ATTR
void DisplayRowInterrupt() {
  alignas(4) uint8_t data[DATA_TRANSFER_LENGTH / 8];
  int data_idx = ALIGN_BUFFER;

  {
    for (int column = 0; column < LINELENGTH; column++) {
      setBitInArray(
          data, data_idx++,
          (framebuffer[((framebuffer_offset + 1) * LINELENGTH + column) %
                       (LINELENGTH * FRAMEBUFFER_COUNT)] >>
           active_line) &
              1);
    }
    for (int i = 0; i < 8; i++) {
      setBitInArray(data, data_idx++, active_line != i);
    }
  }

  {
    for (int column = 0; column < LINELENGTH; column++) {
      setBitInArray(data, data_idx++,
                    (framebuffer[(column + framebuffer_offset * LINELENGTH) %
                                 (LINELENGTH * FRAMEBUFFER_COUNT)] >>
                     active_line) &
                        1);
    }
    for (int i = 0; i < 8; i++) {
      setBitInArray(data, data_idx++, active_line != i);
    }
  }

  // inline version of SPI.transferBytes(data, nullptr, sizeof(data));
  // the interrupt needs to be completely in memory to avoid crashing the device
  // https://stackoverflow.com/a/58131720
  while (SPI1CMD & SPIBUSY) {
    // wait for SPI to be free
  }
  // load data
  uint16_t bits = sizeof(data) * 8 - 1;
  const uint32_t mask = ~((SPIMMOSI << SPILMOSI) | (SPIMMISO << SPILMISO));
  SPI1U1 = ((SPI1U1 & mask) | ((bits << SPILMOSI) | (bits << SPILMISO)));
  volatile uint32_t *fifoPtr = &SPI1W0;
  uint8_t outSize = ((sizeof(data) + 3) / 4);
  uint32_t *dataPtr = (uint32_t *)data;
  while (outSize--) {
    *(fifoPtr++) = *(dataPtr++);
  }
  // initiate transfer
  SPI1CMD |= SPIBUSY;

  active_line++;
  if (active_line >= 7) {
    active_line = 0;
  }
}
