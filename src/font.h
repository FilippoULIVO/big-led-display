#include <Arduino.h>
#include <cstdint>
#include <stddef.h>

extern const uint8_t font_png_start[] asm("_binary__pio_build_font_bin_start");
extern const uint8_t font_png_end[] asm("_binary__pio_build_font_bin_end");

const size_t font_png_size = font_png_end - font_png_start;

uint8_t char_column_data(char c, uint8_t column_index) {
  size_t offset = c * 5 + column_index;
  if (offset >= font_png_size) {
    // out of bounds read
    return 0xff;
  }
  return font_png_start[offset];
}