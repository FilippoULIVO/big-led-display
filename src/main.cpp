#include "display.h"
#include "font.h"
#include "logger.h"
#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <Updater.h>
#include <WebSocketsClient.h>

WebSocketsClient webSocket;

void drawString(String str, unsigned int framebuffer_idx);
void drawStaticString(Line line, String str);

unsigned long buffer_filled_time = 0;
void restart() {
  noInterrupts();
  SPI.end();
  ESP.restart();
}

void display_flash_progress(unsigned int progress, unsigned int total) {
  uint8_t bars = 25 * progress / total;
  drawStaticString(LINE_TOP,
                   String("Flashing ") + 100 * progress / total + " %");
  String barsStr;
  for (int i = 0; i < 25; i++) {
    barsStr += i < bars ? "=" : " ";
  }
  drawStaticString(LINE_BOTTOM, barsStr);
}

bool was_connected = false;
bool using_secondary_server = false;

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length) {

  switch (type) {
  case WStype_DISCONNECTED:
    Logger::printf("[WSc] Disconnected!\n");
    if (Update.isRunning()) {
      Logger::println("Disconnected while flashing");
      restart();
    }

    // 6 hours
    if (millis() > 6 * 60 * 60 * 1000) {
      Logger::println("No connection for more than 6 hours, restarting...");
      restart();
    } else if (was_connected) {
      Logger::println("Disconnected after being connected");
      restart();
    }
    // 1 hour
    else if (!using_secondary_server && millis() >= 1 * 60 * 60 * 1000) {
      drawStaticString(LINE_BOTTOM, (String) "Switching servers...");
      webSocket.begin(SERVER_HOST_SECONDARY, SERVER_PORT, "/display");
    }

    break;
  case WStype_CONNECTED: {
    Logger::printf("[WSc] Connected to url: %s\n", payload);
    was_connected = true;
  } break;
  case WStype_TEXT:
    if (payload) {
      Logger::printf("[WSc] get text: %s\n", payload);
      if (framebuffers_used >= FRAMEBUFFER_COUNT) {
        restart();
        break;
      }
      drawString(String((char *)payload),
                 (framebuffer_offset + framebuffers_used) % FRAMEBUFFER_COUNT);
      framebuffers_used += 1;
      if (framebuffers_used >= FRAMEBUFFER_COUNT) {
        buffer_filled_time = millis();
      }
    }
    break;
  case WStype_BIN:
    Logger::printf("[WSc] get binary length: %u\n", length);
    Logger::printf("flash_remaining: %u\n", Update.remaining());
    Logger::printf("Flash writing %u bytes\n", length);
    if (Update.isRunning()) {
      if (Update.write(payload, length) != length) {
        Logger::println("Failed to write data");
        Logger::println(Update.getErrorString());
        restart();
      }
      Logger::println("Data written to flash");
      display_flash_progress(Update.progress(), Update.size());
      if (Update.isFinished()) {
        if (Update.end()) {
          Logger::println("Update Success, rebooting!");
        } else {
          Logger::println("Update failed");
          Logger::println(Update.getErrorString());
        }
        yield();
        restart();
      }
      break;
    } else if (payload[0] == 0x66 && length == 5) {
      size_t size = 0;
      size = size << 8 | payload[1];
      size = size << 8 | payload[2];
      size = size << 8 | payload[3];
      size = size << 8 | payload[4];
      Logger::printf("OTA incoming %u bytes\n", size);
      bool size_ok = Update.begin(size);

      if (!size_ok) {
        Logger::println("OTA update too big, can not fit");
        Logger::println(Update.getErrorString());
        restart();
      }
      Logger::println("Startig flashing");
      display_flash_progress(Update.progress(), Update.size());
    } else {
      Logger::println("Incorrect flash param");
      restart();
    }
    break;
  case WStype_PING:
  case WStype_PONG:
    break;
  case WStype_ERROR:
    Logger::println("Websocket error, rebooting!");
    restart();
    break;
  case WStype_FRAGMENT_TEXT_START:
  case WStype_FRAGMENT_BIN_START:
  case WStype_FRAGMENT:
  case WStype_FRAGMENT_FIN:
    Logger::println("Websocket fragmentation is unsupported, rebooting!");
    restart();
    break;
  }
}

void setup() {
  SPI.begin();
  // The shift register chip is HEF4094BP
  // Speed set to 3.1Mhz (determined experimentally)
  SPI.beginTransaction(SPISettings(3100000UL, LSBFIRST, SPI_MODE0));
  SPI.setHwCs(true);

  active_line = 0;

  noInterrupts();
  timer1_isr_init();
  timer1_attachInterrupt(DisplayRowInterrupt);
  timer1_enable(TIM_DIV16, TIM_EDGE, TIM_LOOP);
  timer1_write(15000); // 1 / 80_000_000 * 16 * 15000 = 3ms
  interrupts();

  Serial.begin(115200);

  uint32_t flashSize = ESP.getFlashChipRealSize();
  Logger::printf("Flash size: %u bytes\n", flashSize);

  drawStaticString(LINE_TOP, String("Connecting to"));
  drawStaticString(LINE_BOTTOM, String(WIFI_SSID));

  WiFi.hostname("LED-Display");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  if (WiFi.waitForConnectResult(30000) != WL_CONNECTED) {
    Logger::printf("Wifi status %u\n", WiFi.status());
    Logger::println("Failed to connect to WiFi. Resetting...");
    restart();
  }

  ArduinoOTA.onStart([]() { Logger::println("ArduinoOTA onStart"); });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Logger::printf("Progress: %u %u\n", progress, total);
    display_flash_progress(progress, total);
  });

  ArduinoOTA.onEnd([]() { restart(); });

  ArduinoOTA.onError([](ota_error_t error) {
    Logger::printf("ArduinoOTA Error[%u]\n", error);
    restart();
  });
  ArduinoOTA.begin();

  drawStaticString(LINE_TOP, WiFi.localIP().toString() + "V:" + VERSION);
  drawStaticString(LINE_BOTTOM, WiFi.macAddress());

  webSocket.setExtraHeaders("version: " VERSION);

  webSocket.begin(SERVER_HOST, SERVER_PORT, "/display");
  Logger::printf("Connecting to ws://%s:%u", SERVER_HOST, SERVER_PORT);

  webSocket.onEvent(webSocketEvent);

  // This code block will only be compiled for the production environment
  Logger::println("IP address");
  Logger::println(WiFi.localIP().toString());
  Logger::println("Mac address");
  Logger::println(WiFi.macAddress());
  Logger::println("Firmware version");
  Logger::println(VERSION);
}

void loop() {
  webSocket.loop();
  ArduinoOTA.handle();
  if (framebuffers_used > 2 && (millis() - buffer_filled_time) > 1000) {
    framebuffer_offset = (framebuffer_offset + 1) % FRAMEBUFFER_COUNT;
    framebuffers_used -= 1;
    uint8 data = 0;
    bool sent = webSocket.sendBIN(&data, 1);
    Logger::printf("framebuffer_offset: %u, framebuffers_used: %u, sent %u",
                   framebuffer_offset, framebuffers_used, sent);
  }
}

void drawScrollingString(Line line, String str) {
  // TODO reverse scrolling
  int start, letterspace, i;
  letterspace = 6;
  start = 0;
  for (i = 0; i < LINELENGTH; i++) {
    framebuffer[LINE_TOP * LINELENGTH + i] = 0;
  }
  for (unsigned j = 0; j < str.length(); j++) {
    for (i = 0; i < 5; i++) {
      int strbase = j;
      framebuffer[static_cast<int>(line) * LINELENGTH +
                  (strbase + start + i) % LINELENGTH] =
          char_column_data(str.charAt(j), i);
    }
    start += letterspace;
  }
}

void drawString(String str, unsigned int framebuffer_idx) {
  memset(framebuffer + framebuffer_idx * LINELENGTH, 0,
         LINELENGTH * sizeof(uint8_t));
  str = str.substring(0, LINELENGTH / 6);
  int start = (LINELENGTH - str.length() * 6) / 2;
  for (unsigned j = 0; j < str.length(); j++) {
    for (int i = 0; i < 5; i++) {
      framebuffer[framebuffer_idx * LINELENGTH + start + i] =
          char_column_data(str.charAt(j), i);
    }
    start += 6;
  }
}

void drawStaticString(Line line, String str) {
  if (line == LINE_TOP) {
    drawString(str, (framebuffer_offset + 1) % FRAMEBUFFER_COUNT);
  } else {
    drawString(str, framebuffer_offset);
  }
}
